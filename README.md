# mams-vickrey

Example of Vickrey Auction with example MAMS Buyer (Client) and Seller agents. In the default configuration, 4 agents are created at start up:

* `Manager`: 1 of these is created. It is the main agent responsible for managing the auctions
* `SampleClient`: 2 of these are created. They will bid on items for sale
* `SampleSeller`: 1 of these is created. It will submit an item for sale

# Sketch of expected behaviour

When created, the `Manager` creates two resources:

* `/manager/clients`: an endpoint for external services to register as clients. Resources take the form:
```
{
    "id" : "a unique id",
    "notificationUrl" : "a webhook to notify the client that a bidder agent has been created for it",
    "awardUrl" : "a webhook to notify the client that it has won an auction",
    "bidderUrl" : "initially null"
}
```
* `/manager/items`: and endpoint to submit items for sale
```
{
    "item" : "the item to be sold",
    "quantity" : the quantity of the item,
    "uri" : "a webhook to notify the seller that the item was sold (and to whom)"
}
```

The `SampleClient` registers by POSTing to `/manager/clients`.  Upon receipt of a new registration, the `Manager` creates a `Bidder` agent uses the `notificationUrl` to inform the `SampleClient` of a URL that can be used to specify what items the `SampleClient` is interested in.  The `SampleClient` is able to POST information about what it wants to this URL:
```
{
    "item" : "the item to be bought",
    "unitBid" : unit bid price for the item,
    "quantity" : the amount to be bought
}
```

The `SampleSeller` submits an item to be sold by POSTing to `/manager/items`. Upon receipt of a new item for sale, the `Manager` creates an `Auctioneer`, alerts all relevant `Bidder` agents about the auction, and then triggers the auction. To participate, `Bidder` agents must notify the `Auctioneer` of their interest in the auction (this is based on the list of wants submitted to the `Bidder`).

Upon completion, the `Auctioneer` informs the `Manager` of the result of the auction and the `Manager` uses the `awardUrl` and `uri` fields to inform the `SampleSeller` and the winning `SampleClient`.

# Running the demo:

Go to the root folder of the project and type:

```
mvn compile astra:deploy
```

To view the results of the auction, you can query the following URL:

```
http://localhost:9000/seller/awards
```

From here, you can see who won the auction via the `uri` field for each award.

# Running without the sample buyer and seller agents

Step 1: Edit the `Main.astra` file, removing the following code:

```
// Sleep for 10 seconds & create client
system.sleep(1000);
system.createAgent("client", "SampleClient");
system.sleep(1000);
system.createAgent("client2", "SampleClient");
system.sleep(1000);
system.createAgent("seller", "SampleSeller");
```

Step 2: Recompile the codebase:

```
mvn compile
```

Step 3: Run as before