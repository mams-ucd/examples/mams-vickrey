import com.fasterxml.jackson.databind.JsonNode;
import mams.web.HttpResponse;
import mams.JSONBuilder;

agent SampleSeller extends mams.PassiveMAMSAgent {
    module JSONBuilder builder;

    types local {
        formula is(string);
        formula url(string, string);
        formula has(string);
    }

    initial !init();

    rule +!init() {
        MAMSAgent::!init();
        MAMSAgent::!created("base");
        cartago.println("created...");

        // create a webhook to get results of the auctions...
        !has("awards");
        query(url("awards", string uri));

        JsonNode node = builder.createObject();
        builder.addProperty(node, "item", "apples");
        builder.addProperty(node, "quantity", 2);
        builder.addProperty(node, "uri", uri);

        MAMSAgent::!post("http://localhost:9000/manager/items",
                builder.toJsonString(node), HttpResponse response
        );

        if (httpUtils.hasCode(response, 200)) {
            cartago.println("item submitted for sale...");
        }
    }

    rule +!has("awards") : ~has("awards") {
        PassiveMAMSAgent::!listResource("awards", "auction.Award");
        query(artifact("awards", string qname, cartago.ArtifactId id));
        cartago.operation(id, getUri(string uri));
        +url("awards", uri);
        +has("awards");
    }
}